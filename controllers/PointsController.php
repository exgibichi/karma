<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\Points;
use app\models\PointsForm;
use yii\web\NotFoundHttpException;

class PointsController extends BaseController {

    public function actionCreateTestData() {
        set_time_limit(10000);
        // add point for test location
        // create test location
        // generetae 3m points for test loc
        $lat = '63.786733';
        $lng = '100.18325';
        $radius = 100000;
        $point = new Points;
        $point->user_id = 2;
        $point->lat = $lat;
        $point->lng = $lng;
        $point->is_like = 0;
        if(!$point->save()) {
            print_r($point->getErrors());
            die;
        }
        $location = new \app\models\Locations;
        $location->name = '3m точек';
        $location->address = 'Russian Federation, Krasnoyarskiy kray, Evenkiyskiy rayon';
        $location->radius = $radius;
        $location->is_banned = 0;
        $location->point_id = $point->id;
        if(!$location->save()) {
            print_r($location->getErrors());
            die;
        }
        $user_ids = \yii\helpers\ArrayHelper::getColumn(Users::find()->select(['id'])->asArray()->all(), 'id');
        $degree_radius = $radius/111000;
        $c = 0;
        $ci = 0;
        $query = 'insert into points (`id`, `user_id`, `lat`, `lng`, `is_like`, `created_at`) values ';
        $end = end($user_ids);
        foreach($user_ids as $uid) {
            foreach(range(0, 30) as $i) {
                $loc = Points::generatPoint($lat, $lng, $degree_radius);
                $day = mt_rand(1,10)*24*3600;
                $lat = round($loc[0], 6);
                $lng = round($loc[1], 6);
                $is_like = mt_rand(0, 1);
                $created_at = date('Y-m-d H:i:s', time()-$day);
                if($ci == 0) {
                    $ci = 1;
                    $query .= "(NULL, $uid, '$lat', '$lng', $is_like, '$created_at')";
                } else {
                    $query .= ",(NULL, $uid, '$lat', '$lng', $is_like, '$created_at')";
                }
            }
            if($c > 50 || $uid === $end) {
                $c = 0;
                $ci = 0;
                Yii::$app->db->createCommand($query.';')->execute();
                $query = 'insert into points (`id`, `user_id`, `lat`, `lng`, `is_like`, `created_at`) values ';
            }
            $c++;
        }
    }

    public function actionGenPoints() {
        $post = Yii::$app->request->post();
        $lat = $post['lat'];
        $lng = $post['lng'];
        $degree_radius = $post['radius'];
        $cnt = 0;
        foreach(range(1, $post['cnt']) as $i) {
            $loc = Points::generatPoint($lat, $lng, $degree_radius);
            $point = new Points;
            $point->user_id = $post['poi'];
            $point->lat = round($loc[0], 7);
            $point->lng = round($loc[1], 7);
            $point->is_like = (mt_rand()/mt_getrandmax())<($post['likes_percent']/100) ? 1 : 0;
            if(!$point->save()) {
                print_r($point->getErrors());
                die;
            } else {
                $cnt++;
            }
        }
        return json_encode(['cnt' => $cnt]);
    }

    public function actionIndex() {
        return $this->redirect(['view']);
    }

    public function actionView($user_id = false, $location_id = false) {
        $user = false;
        $location = false;
        $model = new PointsForm;
        $model->load(Yii::$app->request->post());
        if($user_id) {
            $user = \app\models\Users::findOne($user_id);
            if($user) {
                $user->getExtremePoints();
                $model->setUserId($user_id);
            }
        }
        if($location_id) {
            $location = \app\models\Locations::findOne($location_id);
            $model->setLocationId($location_id);
        }
        $points = $model->getPoints();
        if(Yii::$app->request->isAjax) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success' => true, 'points' => $points['points'], 'count' => $points['count']];
        }
        return $this->render(
            'index.twig',
            [
                'model' => $model,
                'points' => json_encode($points['points']),
                'center' => json_encode($points['center']),
                'count' => json_encode($points['count']),
                'user' => $user,
                'location' => $location
            ]
        );
    }

    public function actionAddLocationPoint() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new Points();
        $post = Yii::$app->request->post();
        $model->setAttributes($post);
        if ($model->save()) {
            return ['success' => true, 'id' => $model->id];
        } else {
            return ['success' => false, 'error' => $model->getErrors()];
        }
    }

    public function actionBulkAddPoints() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $points = Yii::$app->request->post('points');
        if(!is_array($points) || empty($points)) {
            return ['success' => false, 'saved_points' => []];
        }
        $saved_points = [];
        $users = [];
        foreach($points as $point) {
            if(!isset($point['user_id']) || $point['user_id'] == null) {
                continue;
            }
            $users[] = $point['user_id'];
            if(isset($point['base_id']) && $point['base_id'] != null) {
                $model = Points::findOne($point['base_id']);
                if(!$model) { break; }
                if((int)$point['is_deleted'] === 1) {
                    $model->delete();
                    continue;
                }
                $model->setAttributes($point);
                if($model->save()) {
                    $saved_points[] = $point;
                } else {
                    break;
                }
            } else {
                if((int)$point['is_deleted'] === 1) {
                    continue;
                }
                $map_id = $point['id'];
                $point['id'] = null;
                $model = new Points();
                $model->setAttributes($point);
                if($model->save()) {
                    $point['id'] = $map_id;
                    $point['base_id'] = $model->id;
                    $saved_points[] = $point;
                } else {
                    break;
                }
            }
        }
        if($model) {
            $user = Users::findOne($model->user_id);
            $user->updateLastPoint($model->id);
        }
        Users::updateLikesStat(array_filter(array_unique($users)));
        if (!empty($saved_points)) {
            return ['success' => true, 'saved_points' => $saved_points];
        } else {
            if(isset($model)) {
                return ['success' => false, 'error' => $model->getErrors()];
            } else {
                return ['success' => true, 'saved_points' => []];
            }
        }
    }

    public function actionDeleteLocationPoint() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $point_id = Yii::$app->request->post('point_id', null);
        if($point_id !== null) {
            $model = $this->findModel($point_id);
            if($model->delete()) {
                return ['success' => true];
            } else {
                return ['success' => false, 'error' => $model->getErrors()];
            }
        } else {
            return ['success' => false, 'error' => Yii::t('app', 'No point')];
        }
    }

    protected function findModel($id) {
        if(($model = Points::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
