<?php

namespace app\controllers;

use Yii;
use app\models\Admins;
use app\models\AdminsSearch;
use yii\web\NotFoundHttpException;

class AdminsController extends BaseController {

    public function actionIndex() {
        $searchModel = new AdminsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index.twig', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate() {
        $model = new Admins();
        $load = $model->load(Yii::$app->request->post());
        if ($load) {
            $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
            if($model->save()) {
                return $this->redirect(['index']);
            }
        }
        return $this->render('create.twig', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $old_password = $model->password;
        $load = $model->load(Yii::$app->request->post());
        if ($load) {
            if(!empty($model->password)) {
                $model->password = Yii::$app->getSecurity()->generatePasswordHash($model->password);
            } else {
                $model->password = $old_password;
            }
            if ($model->save()) {
                return $this->redirect(['index']);
            }
        }
        return $this->render('update.twig', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = Admins::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
