<?php

namespace app\controllers;

use Yii;
use app\models\Settings;
use yii\web\Controller;
use yii\filters\AccessControl;

class BaseController extends Controller {

    public function init() {
        $this->view->params['settings'] = Settings::loadAll();
    }

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ],
            ],
        ];
    }

}
