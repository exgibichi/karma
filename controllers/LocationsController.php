<?php

namespace app\controllers;

use Yii;
use app\models\Locations;
use app\models\LocationsSearch;
use yii\web\NotFoundHttpException;

class LocationsController extends BaseController {

    public function actionIndex() {
        $searchModel = new LocationsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index.twig', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        return $this->render('view.twig', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionGeneratePoints($id) {
        $model = $this->findModel($id);
        return $this->render('gen.twig', [
            'model' => $model,
            'points' => json_encode($model->getPoints()),
        ]);
    }

    public function actionCreate() {
        $model = new Locations();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('create.twig', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update.twig', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id) {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = Locations::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
