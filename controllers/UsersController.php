<?php

namespace app\controllers;

use Yii;
use app\models\Users;
use app\models\UsersSearch;
use yii\web\NotFoundHttpException;

class UsersController extends BaseController {

    public function actionCreateTestData() {
        $devices = \yii\helpers\ArrayHelper::getColumn(\app\models\Devices::find()->select(['id'])->asArray()->all(), 'id');
        $images = \yii\helpers\ArrayHelper::getColumn(\app\models\Images::find()->select(['id'])->asArray()->all(), 'id');
        $first_names = [
            'Давид', 'Даниил', 'Данила', 'Данила', 'Дементий', 'Демид', 'Демьян', 'Денис', 'Дмитрий', 'Добромысл', 'Доброслав', 'Евгений', 'Евграф', 'Евдоким', 'Евлампий', 'Евсей', 'Евстафий', 'Евстигней', 'Егор', 'Елизар', 'Елисей', 'Емельян', 'Иван', 'Игнат', 'Игнатий', 'Игорь', 'Измаил', 'Изот', 'Изяслав', 'Иларион', 'Кирилл', 'Лавр', 'Лаврентий', 'Лев', 'Леон', 'Леонид', 'Леонтий',  'Макар', 'Максим', 'Максимилиан', 'Максимильян', 'Мануил', 'Мариан', 'Марк', 'Мартин', 'Мартын', 'Мартьян', 'Матвей', 'Мефодий', 'Мирослав', 'Митрофан', 'Михаил', 'Назар', 'Наркис', 'Натан', 'Наум', 'Нестор', 'Нестер', 'Никандр', 'Никанор'
        ];
        $last_names = [
            'Суханов', 'Миронов', 'Дан', 'Александров', 'Коновалов', 'Шестаков', 'Казаков', 'Ефимов', 'Денисов', 'Хромов', 'Фомин', 'Давыдов', 'Мельников', 'Щербаков', 'Блинов', 'Колесников', 'Карпов', 'Афанасьев', 'Власов', 'Маслов', 'Исаков', 'Тихонов', 'Аксёнов', 'Гаврилов', 'Родионов', 'Котов', 'Горбунов', 'Кудряшов', 'Быков', 'Зуев', 'Третьяков', 'Савельев', 'Летов', 'Рыбаков', 'Суворов', 'Абрамов', 'Воронов', 'Мухин', 'Архипов', 'Доронин', 'Белозёров', 'Рожков', 'Самсонов', 'Мясников', 'Лихачёв', 'Буров', 'Сысоев', 'Трофимов', 'Мартынов', 'Емельянов'
        ];
        $c = 1;
        $query = 'insert into users (`id`, `uid`, `first_name`, `last_name`, `ip`, `device_info_id`, `image_id`,`is_banned`,`is_admin`,`token`,`email`,`created_at`,`updated_at`) values ';
        $indexes = range(1, 100000);
        $end = end($indexes);
        $f = 0;
        foreach($indexes as $i) {
            $user_data = [];
            $did = array_rand($devices);
            $iid = array_rand($images);
            $fnid = array_rand($first_names);
            $lnid = array_rand($last_names);
            $user_data['uid'] = uniqid();
            $user_data['first_name'] = $first_names[$fnid];
            $user_data['last_name'] = $last_names[$lnid];
            $user_data['ip'] = mt_rand(0,255).'.'.mt_rand(0,255).'.'.mt_rand(0,255).'.'.mt_rand(0,255);
            $user_data['device_info_id'] = $did;
            $user_data['image_id'] = $iid;
            $user_data['is_banned'] = mt_rand(0, 1);
            $user_data['is_admin'] = mt_rand(0, 1);
            $user_data['token'] = md5(uniqid());
            $user_data['email'] = uniqid().'@gmail.com';
            $user_data['created_at'] = date('Y-m-d H:i:s');
            $user_data['updated_at'] = date('Y-m-d H:i:s');
            if($f == 0) {
                $f = 1;
                $query .= "(NULL, '".join("','", $user_data)."')";
            } else {
                $query .= ",(NULL, '".join("','", $user_data)."')";
            }
            if($c > 1000 || $i === $end) {
                $c = 0;
                $f = 0;
                Yii::$app->db->createCommand($query.';')->execute();
                $query = 'insert into users (`id`, `uid`, `first_name`, `last_name`, `ip`, `device_info_id`, `image_id`,`is_banned`,`is_admin`,`token`,`email`,`created_at`,`updated_at`) values ';
            }
            $c++;
        }
    }

    public function actionIndex() {
        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index.twig', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPoints($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionToggleBan($id) {
        $model = $this->findModel($id);
        $model->is_banned = (int)!$model->is_banned;
        $model->save();
        return $this->redirect(['index']);
    }

    public function actionMakeAdmin($id) {
        $model = $this->findModel($id);
        $model->is_admin = (int)!$model->is_admin;
        $model->save();
        return $this->redirect(['index']);
    }

    protected function findModel($id) {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
