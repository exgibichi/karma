<?php
$config = [
    'id' => 'api',
    'basePath'  => dirname(__DIR__).'/..',
    'bootstrap'  => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/api/modules/v1', // base path for our module class
            'class' => 'app\api\modules\v1\Api', // Path to module class
        ]
    ],
    'components'  => [
        'urlManager'  => [
            'enablePrettyUrl'  => true,
            'showScriptName'  => false,
            'enableStrictParsing' => false,
            'rules' => [
                [
                    'class'  => 'yii\rest\UrlRule',
                    'controller'  => [
                        'v1/users',
                    ],
                ],
            ],
        ],
        'request' => [
            'enableCookieValidation' => false,
            'parsers' => [
                'application/json'  => 'yii\web\JsonParser',
            ]
        ],
        'cache'  => [
            'class'  => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass'  => 'app\models\Users',
            'enableAutoLogin'  => false,
        ],
        'log' => [
            'traceLevel'  => YII_DEBUG ? 3 : 0,
            'targets'  => [
                [
                    'class'  => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning'],
                    'logFile'  => '@api/runtime/logs/error.log'
                ],
            ],
        ],
        'response' => [
            'class' => 'yii\web\Response',
            'on beforeSend' => function ($event) {
                Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $response = $event->sender;
                if(!$response->isSuccessful) {
                    if(strstr($response->data['message'], 'JEE')) {
                        $data = json_decode(str_ireplace('JEE', '', $response->data['message']), 1);
                        $response->data = [
                            'status' => false,
                            'error_text' =>  $data['message'],
                            'error_code' => $data['code']
                        ];
                    } else {
                        $response->data = [
                            'status' => false,
                            'error_text' =>  $response->data['message'],
                            'error_code' => $response->statusCode
                        ];
                    }
                }
            },
        ],
        'db'  => require(__DIR__ . '/../../config/db.php'),
    ],
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['127.0.0.1', '::1', '83.172.43.57'],
        'historySize' => 1000
    ];
}

return $config;
