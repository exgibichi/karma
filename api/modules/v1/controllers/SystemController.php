<?php

namespace app\api\modules\v1\controllers;
use Yii;
use app\models\Images;
use app\models\ImageUpload;
use yii\web\UploadedFile;
use yii\helpers\Url;

class SystemController extends BaseController {

    public $modelClass = 'app\models\Images';

    public function actionImageUpload() { // todo thumb images, image size limit
        $dir = false;
        $full_path = false;
        $this->getDirForUpload($dir, $full_path);
        $is_valid_dir = $dir && $full_path && !empty($dir);
        if(!$is_valid_dir) {
            return $this->sendError(
                Yii::t('app', 'cant create dir for uploads'),
                6
            );
        }
        $image = UploadedFile::getInstanceByName('image');
        if(!$image) {
            return $this->sendError(Yii::t('app', 'empty image'), 8);
        }
        $image_upload = new ImageUpload;
        $image_upload->image = $image;
        if($image_upload->validate()) {
            $hash = md5($image_upload->image->baseName.time());
            $file_name = $hash.'.'.$image_upload->image->extension;
            $image_upload->image->saveAs($full_path.'/'.$file_name);
            $image_model = new Images;
            $url = str_ireplace('/api/', '/web/', Url::to('@web/images', true));
            $image_model->url = $url.'/'.$dir.'/'.$file_name;
            if($image_model->save()) {
                return [
                    'status' => true,
                    'image_id' => $image_model->id,
                    'image_url' => $image_model->url
                ];
            }
        } else {
            return $this->sendError($image_upload->getErrors(), 7);
        }
        return $this->sendError(Yii::t('app', 'cant upload file'), 5);
    }

    private function getDirForUpload(&$dir, &$full_path) {
        $symbols = range('a', 'z');
        $full_path = Yii::getAlias('@app/web/images/');
        $first_level = $symbols[array_rand($symbols)];
        $full_path = $full_path.'/'.$first_level;
        if(!file_exists($full_path)) {
            mkdir($full_path);
        }
        $second_level = $symbols[array_rand($symbols)];
        $full_path = $full_path.'/'.$second_level;
        if(!file_exists($full_path)) {
            mkdir($full_path);
        }
        $third_level = mt_rand(000, 999);
        $full_path = $full_path.'/'.$third_level;
        if(!file_exists($full_path)) {
            mkdir($full_path);
        }
        $dir = $first_level.'/'.$second_level.'/'.$third_level;
    }
}
