<?php

namespace app\api\modules\v1\controllers;

use Yii;
use app\models\Locations;
use app\models\Settings;
use app\models\Points;
use app\models\Users;

class LocationController extends BaseController {
    
    public $modelClass = 'app\models\Locations';
    
    public function actionIsAvail() {
        $lat = Yii::$app->request->post('lat', null);
        $lng = Yii::$app->request->post('lng', null);
        if(is_null($lat) || is_null($lng)) {
            return $this->sendError(Yii::t('app', 'Wrong coordinates'), 10);
        }
        $lat = $this->chop($lat);
        $lng = $this->chop($lng);
        if($this->isWait()) {
            return $this->sendStatus(false);
        }
        if($this->isMarked($lat, $lng)) {
            return $this->sendStatus(false);
        }
        if($this->isBanned($lat, $lng)) {
            return $this->sendStatus(false);
        }
        return $this->sendStatus(true);
    }
    
    public function actionSetMark() {
        $lat = Yii::$app->request->post('lat', null);
        $lng = Yii::$app->request->post('lng', null);
        $is_like = Yii::$app->request->post('is_like', 0) === 'true' ? 1 : 0;
        if(is_null($lat) || is_null($lng)) {
            return $this->sendError(Yii::t('app', 'Wrong coordinates'), 10);
        }
        $lat = $this->chop($lat);
        $lng = $this->chop($lng);
        if($this->isBanned($lat, $lng)) {
            return $this->sendError(Yii::t('app', 'location is banned'), 2);
        }
        $user = $this->getUser();
        $result = $user->addPoint([$lat, $lng], $is_like);
        if($result === true) {
            return $this->sendStatus(true);
        } else {
            if($result == Users::WAIT) {
                return $this->sendError(Yii::t('app', 'wait'), 11);
            }
            if($result == Users::CANT_ADD_POINT) {
                return $this->sendError(Yii::t('app', 'cant add point'), 12);
            }
        }
        return $this->sendStatus(false, false);
    }
    
    public function actionGetMarks($lat, $lng, $radius, $period_start = null, $period_end = null) {
        if(empty($lat) || empty($lng)) {
            return $this->sendError(Yii::t('app', 'Wrong coordinates'), 10);
        }
        $lat = $this->chop($lat);
        $lng = $this->chop($lng);
        $expired = Settings::getValue('points_expiration')*3600;
        if(!is_null($period_start) && !is_null($period_end)) {
            if(strlen($period_start) > 10) {
                $period_start = substr($period_start, 0, 10);
            }
            if(strlen($period_end) > 10) {
                $period_end = substr($period_end, 0, 10);
            }
            $expired = [
                'start' => date("Y-m-d H:i:s", $period_start),
                'end' => date("Y-m-d H:i:s", $period_end)
            ];
        }
        $points = Points::findByPointAndRadius([$lat, $lng], $radius, $expired);
        $likes_cnt = 0;
        $dislikes_cnt = 0;
        if($points) {
            $likes_cnt = array_sum(array_map(function($p){ return (int)$p->is_like; }, $points));
            $points = array_map(function($p){
                $p->toArray();
                $p['is_like'] = (bool)$p['is_like'];
                return $p;
            }, $points);
            $dislikes_cnt = count($points)-$likes_cnt;
        } else {
            $points = [];
        }
        return [
            'status' => true,
            'likes_cnt' => $likes_cnt,
            'dislikes_cnt' => $dislikes_cnt,
            'marks' => $points
        ];
    }
    
    private function isWait() {
        $user = $this->getUser();
        if($user->isWait()) {
            return true;
        }
        return false;
    }
    
    private function isMarked($lat, $lng) {
        $user = $this->getUser();
        if($user->isMarked($lat, $lng)) {
            return true;
        }
        return false;
    }
    
    private function isBanned($lat, $lng) {
        $banned_locations = Locations::find()
        ->where(['is_banned' => 1])
        ->select(['point_id', 'radius'])
        ->asArray()
        ->all();
        if(empty($banned_locations)) {
            return false;
        }
        $isInside = Points::isInside([$lat, $lng], $banned_locations);
        if($isInside) {
            return true;
        }
        return false;
    }
    
    private function chop($str) {
        $precision = Settings::getValue('precision');
        return substr($str, 0, $precision);
    }
    
}
