<?php

namespace app\api\modules\v1\controllers;

use Yii;
use app\models\Users;
use app\models\Images;
use app\models\Devices;
use app\models\PublicProfile;

class UsersController extends BaseController {

    public $modelClass = 'app\models\Users';

    public function actionIsAuth() {
        $profile = PublicProfile::getByUserId(Yii::$app->user->getId());
        $device['device'] = Yii::$app->request->post('device', null);
        $device['os'] = Yii::$app->request->post('os', null);
        $device['os_version'] = Yii::$app->request->post('os_version', null);
        $device['app_version'] = Yii::$app->request->post('app_version', null);
        if(!empty($device)) {
            $no_device_info = 1;
            $device_info = Devices::find()->where($device)->one();
            if(!$device_info) {
                $device_info = new Devices();
                $device_info->setAttributes($device);
                if($device_info->validate() && $device_info->save()) {
                    $no_device_info = 0;
                }
            } else {
                $no_device_info = 0;
            }
            if(!$no_device_info) {
                $user = $this->getUser();
                $user->device_info_id = $device_info->id;
                $user->save();
            }
        }
        return ['status' => true, 'profile' => $profile];
    }

    public function actionGetProfile() {
        $profile = PublicProfile::getByUserId(Yii::$app->user->getId());
        return ['status' => true, 'profile' => $profile];
    }

    public function actionUpdateProfile() {
        $first_name = Yii::$app->request->post('first_name', null);
        $last_name = Yii::$app->request->post('last_name', null);
        $image_id = Yii::$app->request->post('image_id', null);
        $user = $this->getUser();
        if(!is_null($first_name)) {
            $user->first_name = $first_name;
        }
        if(!is_null($last_name)) {
            $user->last_name = $last_name;
        }
        if(!is_null($image_id)) {
            if(!is_numeric($image_id)) {
                return $this->sendError(
                    Yii::app('app', 'wrong image_id type, must be integer'),
                    3
                );
            }
            if(!Images::findOne($image_id)) {
                return $this->sendError(
                    Yii::app('app', 'wrong image_id, cant find image'),
                    4
                );
            }
            $user->image_id = $image_id;
        }
        if($user->validate() && $user->save()) {
            $profile = PublicProfile::getByUserId(Yii::$app->user->getId());
            return ['status' => true, 'profile' => $profile];
        }
        return $this->sendError(Yii::app('app', 'cant update profile'), 3);
    }

}
