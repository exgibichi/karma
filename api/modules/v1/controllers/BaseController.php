<?php

namespace app\api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use yii\filters\auth\HttpBearerAuth;
use app\models\Users;

class BaseController extends ActiveController {

    public function behaviors() {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
        ];
        return $behaviors;
    }

    public function init() {
        \yii\base\Event::on('\yii\web\User', \yii\web\User::EVENT_AFTER_LOGIN, function ($event) {
            $user = $this->getUser();
            if($user && $user->is_banned) {
                $this->sendError(Yii::t('app', 'User banned'), 18);
            }
        });
    }

    public function actions() {
        return [];
    }

    protected function sendError($message, $code) { // todo make model for error codes
        Yii::$app->response->statusCode = 400;
        throw new \Exception('JEE'.json_encode(['message' => $message, 'code' => $code]));
    }

    protected function sendStatus($success, $status = true) {
        return ['status' => $status, 'success' => $success];
    }

    protected function getUser() {
        static $user;
        if(empty($user) || !$user) {
            if(!Yii::$app->user->getId()) {
                $user = false;
            } else {
                $user = Users::findOne(Yii::$app->user->getId());
            }
        }
        return $user;
    }
}
