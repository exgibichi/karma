<?php

use yii\db\Migration;

class m170304_023301_add_distances_error extends Migration
{
    public function up()
    {
        $sql = "
        INSERT INTO `settings` (`id`, `name`, `alias`, `value`) VALUES (NULL, 'Distances error', 'distances_error', '3');
        ";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170304_023301_add_distances_error cannot be reverted.\n";

        return false;
    }

}
