<?php

use yii\db\Migration;

class m170228_222723_init extends Migration
{
    public function up()
    {
        $sql = "
        CREATE DATABASE IF NOT EXISTS `karma`;
        USE `karma`;

        DROP TABLE IF EXISTS `admins`;
        CREATE TABLE IF NOT EXISTS `admins` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `username` varchar(50) NOT NULL,
          `password` varchar(100) NOT NULL,
          `is_super` tinyint(1) unsigned NOT NULL DEFAULT '0',
          `created_at` datetime NOT NULL,
          `updated_at` datetime NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

        INSERT INTO `admins` (`id`, `username`, `password`, `is_super`, `created_at`, `updated_at`) VALUES
        (1, 'root', '\$2y\$13\$wjuuladVv9p8jM0Ikd0lMeMnIxDHH2AmHUGNNqasgIwtiY5hw/zlm', 1, '2017-02-12 18:59:19', '2017-02-13 06:01:14'),
        (2, 'test', '\$2y\$13\$cxAV2lpqpr7oiWhSuK63X.WIHhO7y8nygyUjEnO1F4xJZOC3GdXc6', 0, '2017-02-12 19:08:39', '2017-02-13 06:27:45');

        DROP TABLE IF EXISTS `devices`;
        CREATE TABLE IF NOT EXISTS `devices` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `os` varchar(50) NOT NULL,
          `os_version` varchar(50) NOT NULL,
          `app_version` varchar(50) NOT NULL,
          `add_info` text,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

        DROP TABLE IF EXISTS `images`;
        CREATE TABLE IF NOT EXISTS `images` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `url` text NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

        DROP TABLE IF EXISTS `locations`;
        CREATE TABLE IF NOT EXISTS `locations` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(250) DEFAULT NULL,
          `address` varchar(500) DEFAULT NULL,
          `radius` float unsigned NOT NULL,
          `point_id` int(10) unsigned NOT NULL,
          `is_banned` tinyint(1) unsigned DEFAULT '0',
          `created_at` datetime NOT NULL,
          `updated_at` datetime NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

        INSERT INTO `locations` (`id`, `name`, `address`, `radius`, `point_id`, `is_banned`, `created_at`, `updated_at`) VALUES
        (1, 'Тестовая локация 1', 'Россия, Москва, поселение Сосенское', 1000, 1, 0, '2017-02-24 19:22:37', '2017-02-24 19:22:37'),
        (2, 'Тестовая локация 2', 'Россия, национальный парк Лосиный Остров', 700, 2, 0, '2017-02-24 20:04:25', '2017-02-24 20:04:25'),
        (3, 'Тестовая локация 3', 'Россия, Москва, Мичуринский проспект', 10000, 3, 0, '2017-02-24 20:09:37', '2017-02-24 20:09:37');

        DROP TABLE IF EXISTS `points`;
        CREATE TABLE IF NOT EXISTS `points` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `user_id` int(10) unsigned NOT NULL,
          `lat` float unsigned NOT NULL,
          `lng` float unsigned NOT NULL,
          `created_at` datetime NOT NULL,
          `is_like` tinyint(1) unsigned NOT NULL DEFAULT '0',
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

        INSERT INTO `points` (`id`, `user_id`, `lat`, `lng`, `created_at`, `is_like`) VALUES
        (1, 2, 55.5544, 37.4469, '2017-02-24 19:22:37', 0),
        (2, 2, 55.8551, 37.9311, '2017-02-24 20:04:24', 0),
        (3, 2, 55.7003, 37.5013, '2017-02-24 20:09:37', 0);

        DROP TABLE IF EXISTS `settings`;
        CREATE TABLE IF NOT EXISTS `settings` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `name` varchar(150) NOT NULL,
          `alias` varchar(150) NOT NULL,
          `value` varchar(500) NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

        INSERT INTO `settings` (`id`, `name`, `alias`, `value`) VALUES
        (2, 'Locations owner id', 'locations_owner_id', '2'),
        (3, 'Bot generator id', 'bot_generator_id', '1'),
        (4, 'Default radius', 'default_radius', '100'),
        (5, 'One degree in meters', 'one_degree_in_meters', '111000'),
        (6, 'Precision', 'precision', '11'),
        (7, 'Default latitude', 'default_lat', '55.76'),
        (8, 'Default longitude', 'default_lng', '37.64'),
        (9, 'Mark frequency', 'mark_frequency', '60');

        DROP TABLE IF EXISTS `users`;
        CREATE TABLE IF NOT EXISTS `users` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `uid` varchar(50) NOT NULL,
          `first_name` varchar(50) NOT NULL,
          `last_name` varchar(50) NOT NULL,
          `image_id` int(10) unsigned DEFAULT NULL,
          `is_banned` tinyint(1) unsigned DEFAULT '0',
          `is_admin` tinyint(1) unsigned DEFAULT '0',
          `ip` varchar(15) NOT NULL,
          `device_info_id` int(10) unsigned DEFAULT NULL,
          `likes_cnt` int(10) unsigned DEFAULT '0',
          `dislikes_cnt` int(10) unsigned DEFAULT '0',
          `email` varchar(250) DEFAULT NULL,
          `token` text,
          `first_point_id` int(10) unsigned DEFAULT NULL,
          `last_point_id` int(10) unsigned DEFAULT NULL,
          `created_at` datetime NOT NULL,
          `updated_at` datetime NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

        INSERT INTO `users` (`id`, `uid`, `first_name`, `last_name`, `image_id`, `is_banned`, `is_admin`, `ip`, `device_info_id`, `likes_cnt`, `dislikes_cnt`, `email`, `token`, `first_point_id`, `last_point_id`, `created_at`, `updated_at`) VALUES
        (1, '1', 'Bot', 'Generator', NULL, 0, 0, '127.0.0.1', NULL, 60, 22, NULL, NULL, NULL, NULL, '2017-02-13 10:15:15', '2017-02-13 15:34:59'),
        (2, '2', 'Locations', 'Owner', NULL, 0, 0, '127.0.0.1', NULL, 0, 0, NULL, NULL, NULL, NULL, '2017-02-13 10:15:15', '2017-02-13 15:34:59'),
        (3, '3', 'Api', 'Tester', 0, 0, 0, '127.0.0.1', NULL, 5, 0, 'tester@gmail.com', '!!!testToken123', 0, 0, '2017-02-27 18:32:41', '2017-03-01 05:19:13');
        ";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170228_222723_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
