<?php

use yii\db\Migration;

class m170301_145507_prev_mig_fix extends Migration
{
    public function up()
    {
        $sql = "
        DROP TABLE IF EXISTS `devices`;
        CREATE TABLE `devices` (
        	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        	`os` VARCHAR(500) NULL DEFAULT NULL,
        	`os_version` VARCHAR(500) NULL DEFAULT NULL,
        	`app_version` VARCHAR(500) NULL DEFAULT NULL,
        	`device` VARCHAR(500) NULL DEFAULT NULL,
        	PRIMARY KEY (`id`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB;
        DROP TABLE IF EXISTS `admins`;
        CREATE TABLE IF NOT EXISTS `admins` (
          `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
          `username` varchar(50) NOT NULL,
          `password` varchar(100) NOT NULL,
          `is_super` tinyint(1) unsigned NOT NULL DEFAULT '0',
          `created_at` datetime NOT NULL,
          `updated_at` datetime NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

        INSERT INTO `admins` (`id`, `username`, `password`, `is_super`, `created_at`, `updated_at`) VALUES
        (1, 'root', '\$2y\$13\$wjuuladVv9p8jM0Ikd0lMeMnIxDHH2AmHUGNNqasgIwtiY5hw/zlm', 1, '2017-02-12 18:59:19', '2017-02-13 06:01:14'),
        (2, 'test', '\$2y\$13\$cxAV2lpqpr7oiWhSuK63X.WIHhO7y8nygyUjEnO1F4xJZOC3GdXc6', 0, '2017-02-12 19:08:39', '2017-02-13 06:27:45');
        ;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170301_145507_prev_mig_fix cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
