<?php

use yii\db\Migration;

class m170408_144201_users_fix extends Migration
{
    public function up()
    {
        $sql = "
        ALTER TABLE `users`
        	ALTER `first_name` DROP DEFAULT,
        	ALTER `last_name` DROP DEFAULT;
        ALTER TABLE `users`
        	CHANGE COLUMN `first_name` `first_name` VARCHAR(50) NULL AFTER `uid`,
        	CHANGE COLUMN `last_name` `last_name` VARCHAR(50) NULL AFTER `first_name`;
            ";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170408_144201_users_fix cannot be reverted.\n";

        return false;
    }

}
