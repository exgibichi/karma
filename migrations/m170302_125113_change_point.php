<?php

use yii\db\Migration;

class m170302_125113_change_point extends Migration
{
    public function up()
    {
        $sql = "
        ALTER TABLE `points`
        ALTER `lat` DROP DEFAULT,
        ALTER `lng` DROP DEFAULT;
        ALTER TABLE `points`
        CHANGE COLUMN `lat` `lat` VARCHAR(11) NOT NULL AFTER `user_id`,
        CHANGE COLUMN `lng` `lng` VARCHAR(11) NOT NULL AFTER `lat`;
        INSERT INTO `settings` (`id`, `name`, `alias`, `value`) VALUES (NULL, 'Points expiration in hours', 'points_expiration', '24');
        INSERT INTO `users` (`id`, `uid`, `first_name`, `last_name`, `image_id`, `is_banned`, `is_admin`, `ip`, `device_info_id`, `likes_cnt`, `dislikes_cnt`, `email`, `token`, `first_point_id`, `last_point_id`, `created_at`, `updated_at`) VALUES (NULL, '4', 'Banned', 'user', NULL, 1, 0, '127.0.0.1', 0, 0, 0, 'banned@gmail.com', '!!!bannedToken', NULL, NULL, '2017-03-02 06:09:11', '2017-03-02 06:12:06');
        ";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170302_125113_change_point cannot be reverted.\n";

        return false;
    }

}
