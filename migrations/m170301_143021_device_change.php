<?php

use yii\db\Migration;

class m170301_143021_device_change extends Migration
{
    public function up()
    {
        $sql = "
        DROP TABLE IF EXISTS `admins`;
        CREATE TABLE `devices` (
        	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
        	`os` VARCHAR(500) NULL DEFAULT NULL,
        	`os_version` VARCHAR(500) NULL DEFAULT NULL,
        	`app_version` VARCHAR(500) NULL DEFAULT NULL,
        	`device` VARCHAR(500) NULL DEFAULT NULL,
        	PRIMARY KEY (`id`)
        )
        COLLATE='utf8_general_ci'
        ENGINE=InnoDB
        ;";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170301_143021_device_change cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
