<?php

use yii\db\Migration;

class m170320_112248_add_limit_setting extends Migration
{
    public function up()
    {
        $sql = "
        INSERT INTO `settings` (`id`, `name`, `alias`, `value`) VALUES
        (13, 'Show point limit', 'show_point_limit', '300');
        ";
        $this->execute($sql);
    }

    public function down()
    {
        echo "m170320_112248_add_limit_setting cannot be reverted.\n";

        return false;
    }

}
