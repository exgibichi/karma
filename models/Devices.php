<?php

namespace app\models;

use Yii;

class Devices extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'devices';
    }

    public function rules() {
        return [
            [['device', 'os', 'os_version', 'app_version'], 'string', 'max' => 500],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'os' => Yii::t('app', 'Os'),
            'os_version' => Yii::t('app', 'Os Version'),
            'app_version' => Yii::t('app', 'App Version'),
            'device' => Yii::t('app', 'Device'),
        ];
    }
}
