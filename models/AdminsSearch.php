<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Admins;

class AdminsSearch extends Admins {

    public function rules() {
        return [
            [['id', 'is_super'], 'integer'],
            [['username', 'password', 'created_at', 'updated_at'], 'safe'],
        ];
    }
    
    public function search($params) {
        $query = Admins::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'is_super' => $this->is_super,
        ]);
        $query->andFilterWhere(['like', 'username', $this->username]);
        return $dataProvider;
    }
}
