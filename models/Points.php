<?php
namespace app\models;

use Yii;

class Points extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'points';
    }

    public function rules() {
        return [
            [['user_id', 'lat', 'lng'], 'required'],
            [['user_id', 'is_like'], 'integer'],
            [['lat', 'lng'], 'number'],
            [['created_at'], 'safe'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'lat' => Yii::t('app', 'Lat'),
            'lng' => Yii::t('app', 'Lng'),
            'created_at' => Yii::t('app', 'Created At'),
            'is_like' => Yii::t('app', 'Is Like'),
        ];
    }

    public static function generatPoint($lat, $lng, $radius) {
        $theta = (mt_rand()/mt_getrandmax()) * 2 * M_PI;
        $distance = (mt_rand()/mt_getrandmax()) * $radius;
        $x = $distance * cos($theta) + $lat;
        $y = $distance * sin($theta) + $lng;
        if($y > 180 || $y < -180) {
            $y = $y % 180;
        }
        if($x > 90 || $x < -90) {
            $x = $x % 90;
        }
        return [$x, $y];
    }

    public static function add($coords, $is_like, $user_id) {
        $point = new Points;
        $point->lat = $coords[0];
        $point->lng = $coords[1];
        $point->is_like = (int)$is_like;
        $point->user_id = $user_id;
        if($point->save()) {
            return $point->id;
        }
    }

    public static function findByPointAndRadius(
        $coords,
        $radius,
        $expired = false,
        $user_id = false,
        $limit = false
    ) {
        $locations_ids = \yii\helpers\ArrayHelper::getColumn(
            Locations::find()
            ->select('point_id')
            ->asArray()
            ->all(),
            'point_id'
        );
        $query = Points::find()
        ->select(['lat', 'lng', 'is_like'])
        ->where(['not in', 'id', $locations_ids])
        ->andWhere([
            '<=',
            'ST_Distance_Sphere(POINT(lng, lat), POINT('.$coords[1].', '.$coords[0].'))',
            $radius
        ]);
        if($expired) {
            if(is_array($expired)) {
                $query->andWhere(['between', 'created_at', $expired['start'], $expired['end']]);
            } else {
                $expression = new \yii\db\Expression('NOW() - interval :interval second', ['interval' => $expired]);
                $query->andWhere(['>', 'created_at', $expression]);
            }
        }
        if($user_id) {
            $query->andWhere(['user_id' => $user_id]);
        }
        if($limit) {
            $query->limit($limit);
        }
        return $query->all();
    }

    public static function isInside($coords, $areals) {
        foreach($areals as $areal) {
            $point = Points::findOne($areal['point_id']);
            if(!$point) { continue; }
            $radius = Points::find()
            ->select(['radius' => 'ST_Distance_Sphere(POINT('.$point->lng.', '.$point->lat.'), POINT('.$coords[1].', '.$coords[0].'))'])
            ->limit(1)
            ->asArray()
            ->one();
            if($radius['radius'] < $areal['radius']) {
                return true;
            }
        }
    }

    public function beforeSave($insert) {
        if(!$this->created_at) {
            $this->created_at = date('Y-m-d H:i:s');
        }
        return parent::beforeSave($insert);
    }
}
