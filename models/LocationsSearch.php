<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Locations;

class LocationsSearch extends Locations {

    public function rules() {
        return [
            [['radius', 'is_banned'], 'integer'],
            [['name', 'address', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    public function getGeneratePointsButton() {
        return function ($url,$model) {
            return \yii\helpers\Html::a(
                '<span class="fa fa-map-marker" style="font-size:20pt;"></span>',
                $url
            );
        };
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Locations::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'radius' => $this->radius,
            'is_banned' => $this->is_banned,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'address', $this->address]);
        return $dataProvider;
    }
}
