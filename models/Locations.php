<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

class Locations extends Model {
    private $point;

    public static function tableName() {
        return 'locations';
    }

    public function rules() {
        return [
            [['point_id', 'radius'], 'required'],
            [['radius', 'point_id', 'is_banned'], 'integer'],
            [['base_points_cnt', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 250],
            [['address'], 'string', 'max' => 500],
        ];
    }

    public function countPoints() {
        $point = $this->getPoint();
        $points = [];

        if(empty($point)) { return 0; }

        $points_cnt = Points::find()
        ->select(['base_id' => 'id', 'lat', 'lng', 'is_like', 'points_owner_id' => 'user_id'])
        ->where(['user_id' => Settings::getValue('bot_generator_id')])
        ->andWhere([
            '<=',
            'ST_Distance_Sphere(POINT(lng, lat), POINT('.$point['lng'].', '.$point['lat'].'))',
            $this->radius
        ])
        ->count();
        return $points_cnt;
    }

    public function getPoints() {
        $point = $this->getPoint();
        $points = [];

        if(empty($point)) { return []; }

        $points = Points::find()
        ->select(['base_id' => 'id', 'lat', 'lng', 'is_like', 'points_owner_id' => 'user_id'])
        ->where(['user_id' => Settings::getValue('bot_generator_id')])
        ->andWhere([
            '<=',
            'ST_Distance_Sphere(POINT(lng, lat), POINT('.$point['lng'].', '.$point['lat'].'))',
            $this->radius
        ])
        ->limit(Settings::getValue('show_point_limit'))
        ->asArray()
        ->all();
        return !empty($points) ? $points : [];
    }

    public function toJSON() {
        $model = $this->toArray();
        $point = $this->getPoint();
        $model['base_points_cnt'] = $this->countPoints();
        $model['lat'] = $point['lat'];
        $model['lng'] = $point['lng'];
        return json_encode($model);
    }

    public function getPoint() {
        if(empty($this->point)) {
            $this->point = Points::find()
            ->where(['id' => $this->point_id])
            ->select(['lat', 'lng'])
            ->asArray()
            ->one();
        }
        return $this->point;
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'address' => Yii::t('app', 'Address'),
            'radius' => Yii::t('app', 'Radius'),
            'point_id' => Yii::t('app', 'Point ID'),
            'is_banned' => Yii::t('app', 'Is Banned'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }
}
