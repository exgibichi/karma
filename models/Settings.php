<?php

namespace app\models;

use Yii;

class Settings extends Model {

    public static function tableName() {
        return 'settings';
    }

    public static function getValue($name) {
        $settings = self::loadAll();
        if(isset($settings[$name])) {
            return $settings[$name];
        }
    }

    public static function loadAll() {
        static $settings;
        if(empty($settings)) {
            $settings = \yii\helpers\ArrayHelper::map(
                self::find()->select(['alias', 'value'])->asArray()->all(),
                'alias',
                'value'
            );
        }
        return $settings;
    }

    public function rules() {
        return [
            [['name', 'value'], 'required'],
            [['name'], 'string', 'max' => 150],
            [['alias'], 'string', 'max' => 150],
            [['value'], 'string', 'max' => 500],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'alias' => Yii::t('app', 'Alias'),
            'value' => Yii::t('app', 'Value'),
        ];
    }
}
