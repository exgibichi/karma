<?php
namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

class Users extends Model implements \yii\web\IdentityInterface  {
    const WAIT = 1;
    const CANT_ADD_POINT = 1;
    public $first_point;
    public $last_point;

    public function behaviors() {
        return [
            [
                'class' => TimestampBehavior::className(),
                'value' => new \yii\db\Expression('NOW()')
            ]
        ];
    }

    public static function tableName() {
        return 'users';
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        Yii::trace(['token' => $token], __METHOD__);
        $token = trim($token);
        $user = static::findOne(['token' => $token]);
        if(!$user) {
            Yii::trace('user not found', __METHOD__);
            $firebase = \Firebase::fromServiceAccount(Yii::getAlias('@app/config').'/google-service-account.json');
            try {
                $idToken =  $firebase->getTokenHandler()->verifyIdToken($token);
            } catch (\Exception $e) {
                Yii::trace(['firebase_exception' => $e->getMessage()], __METHOD__);
                return;
            }
            $new_user = self::getDataFromToken($idToken, $token);
            Yii::trace(['new_user' => $new_user], __METHOD__);
            $user = static::findOne(['uid' => $new_user['uid']]);
            if(!$user) {
                Yii::trace('user not found again', __METHOD__);
                $user = new Users;
            }
            $user->setAttributes($new_user);
            if($user->validate() && $user->save()) {
                Yii::trace('new user saved', __METHOD__);
                return $user;
            } else {
                Yii::trace(['user not saved' => $user->getErrors()], __METHOD__);
                return;
            }
        }
        return $user;
    }

    public static function getDataFromToken($idToken, $token) {
        $new_user = [];
        $new_user['token'] = $token;
        $new_user['ip'] = Yii::$app->request->getUserIP();
        $claims = $idToken->getClaims();
        $new_user['uid'] = (string)\yii\helpers\ArrayHelper::getValue($claims, 'user_id', '');
        $name = explode(' ', trim((string)\yii\helpers\ArrayHelper::getValue($claims, 'name', '')));
        if(count($name) > 1) {
            $new_user['first_name'] = trim($name[0]);
            $name[0] = '';
            $new_user['last_name'] = trim(join(' ', $name));
        } else {
            $new_user['first_name'] = trim($name[0]);
        }
        $new_user['email'] = (string)\yii\helpers\ArrayHelper::getValue($claims, 'email', '');
        $picture = (string)\yii\helpers\ArrayHelper::getValue($claims, 'picture', '');
        if(!empty($picture)) {
            $has_image = 0;
            $image = Images::find()->where(['url' => $picture])->one();
            if(!$image) {
                $image = new Images;
                $image->url = $picture;
                if($image->validate() && $image->save()) {
                    $has_image = 1;
                }
            } else {
                $has_image = 1;
            }
            if($has_image) {
                $new_user['image_id'] = $image->id;
            }
        }
        return $new_user;
    }

    public static function findByUsername($username) {
        return static::findOne(['username' => $username]);
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public function getId() {
        return $this->id;
    }

    public function getDevice() {
        if($this->device_info_id) {
            $device = Devices::findOne($this->device_info_id);
            if($device) {
                return $device->device;
            }
        }
    }

    public function getImage() {
        if($this->image_id) {
            $image = Images::findOne($this->image_id);
            if($image) {
                return "<img width=\"50px\" src=\"{$image->url}\"/>";
            }
        }
    }

    public function getBigImage() {
        if($this->image_id) {
            $image = Images::findOne($this->image_id);
            if($image) {
                return "<img class=\"col-md-3\" src=\"{$image->url}\"/>";
            }
        }
    }

    public function updateLastPoint($point_id) {
        if(!$this->first_point_id) {
            $this->first_point_id = $point_id;
        }
        $this->last_point_id = $point_id;
        $this->save();
    }

    private function checkFrequency() {
        if(is_null($this->last_point_id)) {
            return true;
        }
        $last_point = Points::find()
        ->select('created_at')
        ->where(['id' => $this->last_point_id])
        ->one();
        if(!$last_point) {
            return true;
        }
        $last_point_time = strtotime($last_point->created_at);
        $frequency = Settings::getValue('mark_frequency');
        return $last_point_time + $frequency < time();
    }

    public function addPoint($coords, $is_like) {
        if(!$this->checkFrequency()) {
            return self::WAIT;
        }
        $point_id = Points::add($coords, $is_like, $this->id);
        if($point_id) {
            $this->updateLastPoint($point_id);
            self::updateLikesStat([$this->id]);
        } else {
            return self::CANT_ADD_POINT;
        }
        return true;
    }

    public static function updateLikesStat($user_ids) {
        foreach($user_ids as $user_id) {
            $likes_cnt = Points::find()
            ->where(['user_id' => $user_id, 'is_like' => 1])
            ->count();
            $dislikes_cnt = Points::find()
            ->where(['user_id' => $user_id, 'is_like' => 0])
            ->count();
            self::updateAll(
                ['likes_cnt' => $likes_cnt, 'dislikes_cnt' => $dislikes_cnt],
                ['id' => $user_id]
            );
        }
    }

    public function isWait() {
        if(!$this->checkFrequency()) {
            return true;
        }
    }

    public function isMarked($lat, $lng) {
        $radius = Settings::getValue('distances_error');
        $point = Points::findByPointAndRadius([$lat, $lng], $radius, false, $this->id, 1);
        if($point) {
            return true;
        }
    }

    public function getExtremePoints() {
        if($this->first_point_id) {
            $point = Points::findOne($this->first_point_id);
            if($point) {
                $this->first_point = [
                    'coords' => [$point->lat, $point->lng],
                    'is_like' => $point->is_like
                ];
            } else {
                $this->first_point = [];
            }
        } else {
            $this->first_point = [];
        }
        if($this->last_point_id) {
            $point = Points::findOne($this->last_point_id);
            if($point) {
                $this->last_point = [
                    'coords' => [$point->lat, $point->lng],
                    'is_like' => $point->is_like
                ];
            } else {
                $this->last_point = [];
            }
        } else {
            $this->last_point = [];
        }
    }

    public function rules() {
        return [
            [['uid', 'ip'], 'required'],
            [['is_banned', 'is_admin', 'device_info_id', 'likes_cnt', 'dislikes_cnt', 'first_point_id', 'last_point_id'], 'integer'],
            [['image_id', 'is_banned', 'is_admin', 'device_info_id', 'likes_cnt', 'dislikes_cnt', 'first_point_id', 'last_point_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['uid'], 'string', 'max' => 50],
            [['uid', 'first_name', 'last_name'], 'string', 'max' => 50],
            [['ip'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 250],
            [['token'], 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'uid' => Yii::t('app', 'Uid'),
            'is_banned' => Yii::t('app', 'Is Banned'),
            'is_admin' => Yii::t('app', 'Is Admin'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'image_id' => Yii::t('app', 'Image ID'),
            'ip' => Yii::t('app', 'Ip'),
            'device_info_id' => Yii::t('app', 'Device Info ID'),
            'likes_cnt' => Yii::t('app', 'Likes Cnt'),
            'dislikes_cnt' => Yii::t('app', 'Dislikes Cnt'),
            'email' => Yii::t('app', 'Email'),
            'token' => Yii::t('app', 'Token'),
            'first_point_id' => Yii::t('app', 'First Point ID'),
            'last_point_id' => Yii::t('app', 'Last Point ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }
}
