<?php

namespace app\models;

use Yii;

class ImageUpload extends \yii\base\Model {
    public $image;
    public function rules() {
        return [
            [['image'], 'file', 'extensions' => ['jpg', 'jpeg', 'bmp', 'png']],
        ];
    }
}
