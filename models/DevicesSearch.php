<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Devices;

class DevicesSearch extends Devices {

    public function rules() {
        return [
            [['id'], 'integer'],
            [['os', 'os_version', 'app_version', 'device'], 'safe'],
        ];
    }

    public function getFindUsersButton() {
        return function ($url,$model) {
            return \yii\helpers\Html::a(
                '<span class="fa fa-users text-primary" style="font-size:20pt;"></span>',
                \yii\helpers\Url::toRoute([
                    '/users/index',
                    'device_info_id' => $model->id
                ])
            );
        };
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Devices::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['like', 'os', $this->os])
        ->andFilterWhere(['like', 'os_version', $this->os_version])
        ->andFilterWhere(['like', 'app_version', $this->app_version])
        ->andFilterWhere(['like', 'device', $this->device]);
        return $dataProvider;
    }
}
