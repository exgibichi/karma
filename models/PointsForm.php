<?php

namespace app\models;

use Yii;
use yii\base\Model;

class PointsForm extends Model {
    public $center_coords;
    public $radius;
    public $time_filter;
    public $from;
    public $to;
    public $user_id = false;
    public $location_id = false;
    public $show_bots_points = false;
    public $show_likes = true;
    public $show_dislikes = true;
    public $time_filters = [];

    public function init() {
        $filters = [
            Yii::t('app', 'today') => 1,
            Yii::t('app', 'yesterday') => 1,
            Yii::t('app', 'last 3 days') => 1,
            Yii::t('app', 'last week') => 1,
            Yii::t('app', 'last month') => 1,
            Yii::t('app', 'custom date range') => 1
        ];
        $this->time_filters = array_keys($filters);
    }

    public function setUserId($user_id) {
        $this->user_id = $user_id;
    }

    public function setLocationId($location_id) {
        $this->location_id = $location_id;
    }

    public function getPoints() {
        $query = Points::find();
        $query = $this->getQueryByPeriod($query);
        $query = $this->getQueryByType($query);
        $query = $this->getQueryByPointsAndRadius($query);
        $count = $query->count();
        $points = $query
        ->select(['lat', 'lng', 'is_like'])
        ->orderBy('lat DESC, lng DESC')
        ->limit(Settings::getValue('show_point_limit'))
        ->asArray()
        ->all();
        if($points) {
            $first_point = $points[0];
            $last_point = end($points);
            $center = $this->getCenter($first_point, $last_point);
            return ['center' => $center, 'points' => $points, 'count' => $count];
        }
        return ['center' => [], 'points' => [], 'count' => 0];
    }

    private function getCenter($fp, $lp) {
        $lat = round(($lp['lat'] + $fp['lat'])/2, 8);
        $lng = round(($lp['lng'] + $fp['lng'])/2, 8);
        return [$lat, $lng];
    }

    private function getQueryByPointsAndRadius($query) {
        if($this->location_id) {
            $location = Locations::findOne($this->location_id);
            if($location && $location->point) {
                $this->center_coords = $location->point['lat'].','.$location->point['lng'];
                $this->radius = $location->radius;
            }
        }
        if(!empty($this->center_coords) && !empty($this->radius)) {
            $center_coords = explode(',', $this->center_coords);
            $query->andWhere([
                '<=',
                'ST_Distance_Sphere(
                    POINT(lng, lat),
                    POINT(
                        '.(float)$center_coords[1].',
                        '.(float)$center_coords[0].'
                        )
                        )',
                        round($this->radius, 4)
                ]
            );
        }
        return $query;
    }

    private function getQueryByType($query) {
        if($this->user_id) {
            $query->andWhere(['user_id' => $this->user_id]);
        }
        if($this->show_bots_points) {
            $query->andWhere(['user_id' => Settings::getValue('bot_generator_id')]);
        } elseif($this->user_id != Settings::getValue('bot_generator_id')) {
            $query->andWhere(['!=', 'user_id', Settings::getValue('bot_generator_id')]);
        }
        if(!$this->show_likes || !$this->show_dislikes) {
            if($this->show_likes) {
                $query->andWhere(['is_like' => 1]);
            }
            if($this->show_dislikes) {
                $query->andWhere(['is_like' => 0]);
            }
        }
        if(!$this->show_likes && !$this->show_dislikes) {
            $query->where('0=1');
        }
        return $query;
    }

    private function getQueryByPeriod($query) {
        $periods['today'] = strtotime('24:00:00');
        $periods['yesterday'] = strtotime('-1 day', $periods['today']);
        $periods['day_before_yesterday'] = strtotime('-1 day', $periods['yesterday']);
        $periods['tree_days_before_today'] = strtotime('-3 day', $periods['today']);
        $periods['last_week'] = strtotime('-1 week', $periods['today']);
        $periods['last_month'] = strtotime('-1 month', $periods['today']);
        $periods = array_map(function($p) { return date('Y-m-d H:i:s', $p); }, $periods);
        switch($this->time_filter) {
            case 0:
            $query->andWhere(['>=', 'created_at', $periods['yesterday']]);
            break;
            case 1:
            $query->andWhere(['<=', 'created_at', $periods['yesterday']]);
            $query->andWhere(['>=', 'created_at', $periods['day_before_yesterday']]);
            break;
            case 2:
            $query->andWhere(['>=', 'created_at', $periods['tree_days_before_today']]);
            break;
            case 3:
            $query->andWhere(['>=', 'created_at', $periods['last_week']]);
            break;
            case 4:
            $query->andWhere(['>=', 'created_at', $periods['last_month']]);
            break;
            case 5:
            $query = $this->getQueryByTime($query);
            break;
        }
        return $query;
    }

    private function getQueryByTime($query) {
        if(!empty($this->from) || !empty($this->to)) {
            if(!empty($this->from)) {
                $query->andWhere(['>=', 'created_at', date('Y-m-d H:i:s', strtotime($this->from))]);
            }
            if(!empty($this->to)) {
                $query->andWhere(['<=', 'created_at', date('Y-m-d H:i:s', strtotime($this->to))]);
            }
        }
        return $query;
    }

    public function rules() {
        return [
            [
                [
                    'center_coords',
                    'radius',
                    'time_filter',
                    'from',
                    'to',
                    'show_bots_points',
                    'show_likes',
                    'show_dislikes',
                    'time_filters'
                ],
                'safe'
            ]
        ];
    }

}
