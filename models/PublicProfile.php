<?php
namespace app\models;

use Yii;

class publicProfile extends Model {

    public static function getByUserId($user_id) {
        $user = Users::findOne($user_id);
        $fields = [
            'is_banned' => [
                'func' => function($is_banned) {
                    return (bool)$is_banned;
                }
            ],
            'first_name',
            'last_name',
            'image_id' => [
                'func' => function($image_id) {
                    if(!$image_id) {
                        return null;
                    }
                    $image = Images::findOne($image_id);
                    if($image) {
                        return $image->url;
                    }
                },
                'field_new_name' => 'image_url'
            ],
            'likes_cnt',
            'dislikes_cnt',
            'email'
        ];
        $profile = [];
        foreach($fields as $key => $field) {
            if(is_numeric($key)) {
                $profile[$field] = $user->$field;
            } else {
                $field_name = isset($field['field_new_name']) ? $field['field_new_name'] : $key;
                $profile[$field_name] = $field['func']($user->$key);
            }
        }
        return $profile;
    }
}
