<?php

namespace app\models;

use Yii;

class Images extends \yii\db\ActiveRecord {

    public static function tableName() {
        return 'images';
    }

    public function rules() {
        return [
            [['url'], 'required'],
            [['url'], 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
        ];
    }
}
