<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Users;

class UsersSearch extends Users {
    public $device_search;

    public function rules() {
        return [
            [['id', 'is_banned', 'is_admin', 'likes_cnt', 'dislikes_cnt'], 'integer'],
            [['uid', 'device_search', 'email', 'first_name', 'last_name', 'ip', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    public function getMakeAdminButton() {
        return function ($url,$model) {
            return \yii\helpers\Html::a(
                '<span class="fa fa-users text-success" style="font-size:20pt;"></span>',
                $url
            );
        };
    }

    public function getToggleBanButton() {
        return function ($url,$model) {
            return \yii\helpers\Html::a(
                '<span class="fa fa-ban text-danger" style="font-size:20pt;"></span>',
                $url
            );
        };
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Users::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $device_info_id = null;
        if(isset($params['device_info_id'])) {
            $device = Devices::findOne($params['device_info_id']);
            if($device) {
                $device_info_id = $device->id;
            } else {
                $device_info_id = -1;
            }
        }
        if($this->device_search) {
            $device = Devices::find()->where(['like', 'device', $this->device_search])->select(['id'])->one();
            if($device) {
                $device_info_id = $device->id;
            } else {
                $device_info_id = -1;
            }
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'is_banned' => $this->is_banned,
            'is_admin' => $this->is_admin,
            'device_info_id' => $device_info_id,
            'likes_cnt' => $this->likes_cnt,
            'dislikes_cnt' => $this->dislikes_cnt,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);
        $query->andFilterWhere(['like', 'uid', $this->uid])
        ->andFilterWhere(['like', 'first_name', $this->first_name])
        ->andFilterWhere(['like', 'last_name', $this->last_name])
        ->andFilterWhere(['like', 'email', $this->last_name])
        ->andFilterWhere(['like', 'ip', $this->ip]);
        return $dataProvider;
    }
}
