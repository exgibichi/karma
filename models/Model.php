<?php

namespace app\models;

use Yii;

class Model extends \yii\db\ActiveRecord {

    public function getUpdateButton() {
        return function($url, $model) {
            return \yii\helpers\Html::a(
                '<span class="fa fa-pencil-square" style="font-size:20pt;"></span>',
                $url
            );
        };
    }

    public function getViewButton() {
        return function($url, $model) {
            $model_name = explode('\\', $model::className());
            $model_name = end($model_name);
            $param_name = [
                'Locations' => 'location_id',
                'Users' => 'user_id'
            ];
            return \yii\helpers\Html::a(
                '<span class="fa fa-eye" style="font-size:20pt;"></span>',
                \yii\helpers\Url::toRoute([
                    '/points/view',
                    $param_name[$model_name] => $model->id
                ])
            );
        };
    }

    public function getDeleteButton() {
        return function($url, $model) {
            return \yii\helpers\Html::a(
                '<span class="fa fa-trash text-danger" style="font-size:20pt;"></span>',
                $url
            );
        };
    }
}
