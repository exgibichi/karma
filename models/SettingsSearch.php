<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Settings;

class SettingsSearch extends Settings {

    public function rules() {
        return [
            [['id'], 'integer'],
            [['name', 'alias', 'value'], 'safe'],
        ];
    }

    public function scenarios() {
        return Model::scenarios();
    }

    public function search($params) {
        $query = Settings::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        $query->andFilterWhere([
            'id' => $this->id,
        ]);
        $query->andFilterWhere(['like', 'name', $this->name])
        ->andFilterWhere(['like', 'value', $this->value])
        ->andFilterWhere(['like', 'alias', $this->alias]);
        return $dataProvider;
    }
}
