ymaps.ready(function () {
    loadMap();
    loadCircle();
    loadPointsFromBase();
    addPointByClick();
});

function loadMap() {
    map = new ymaps.Map("map", {
        center: [pack.lat, pack.lng],
        zoom: 10,
        controls: ["zoomControl", "fullscreenControl", "typeSelector"]
    });
}

function addPointByClick() {
    map.events.add('click', function (e) {
        var coords = e.get('coords');
        var lat = coords[0].toPrecision(precision);
        var lng = coords[1].toPrecision(precision);
        addMark([lat, lng], 1, null, points_owner_id);
    });
}

function createCallback(id) {
    return function() {
        if(debug) {
            console.log('click on point id:', id);
        }
        deletePointById(id);
        updatePoints();
    };
}

function addMark(coord, is_like, base_id = null, points_owner_id = null) {
    is_like = parseInt(is_like);
    lat = parseFloat(coord[0]);
    lng = parseFloat(coord[1]);
    var point = new ymaps.Placemark([lat, lng], {}, {
        preset: is_like ? 'islands#greenCircleDotIcon' : 'islands#redCircleDotIcon'
    });
    map.geoObjects.add(point);
    if(debug) {
        console.log("map add mark: ", lat, lng);
    }
    i++;
    points.push({
        id: i,
        base_id: base_id,
        lat: lat,
        lng: lng,
        is_like: is_like,
        is_deleted: 0,
        user_id: points_owner_id,
        point_obj: point
    });
    point.events.add('click', createCallback(i));
    updatePoints();
}

function loadCircle() {
    circle = new ymaps.Circle([[pack.lat, pack.lng], pack.radius], {}, {
        fillColor: "#DB709377",
        strokeColor: "#990066",
        interactivityModel: "default#silent",
        strokeOpacity: 0.8,
        strokeWidth: 1
    });
    map.geoObjects.add(circle);
    var bounds = circle.geometry._bounds;
    map.setBounds(bounds, { checkZoomRange: true });
    getBoundsForGen();
}

function loadPointsFromBase() {
    if(base_points.length > 0) {
        $.each(base_points, function(i, v) {
            addMark([v.lat, v.lng], v.is_like, v.base_id, v.points_owner_id);
        });
    }
}

function getBoundsForGen() {
    /*
    lat_max = circle.geometry._bounds[0][0];
    lat_min = circle.geometry._bounds[1][0];
    lng_max = circle.geometry._bounds[0][1];
    lng_min = circle.geometry._bounds[1][1];
    lat_mid = ((lat_min + lat_min)/2);
    lng_mid = ((lng_min + lng_max)/2);
    lat_diff = lat_min-lat_max;
    lng_diff = lng_min-lng_max;
    radius = lat_mid-pack.lat;
    */
}
