ymaps.ready(function () {
    var map = new ymaps.Map("map", {
        center: [pack.lat, pack.lng],
        zoom: 15,
        controls: ["zoomControl", "fullscreenControl", "geolocationControl", "searchControl", "typeSelector"]
    });
    var last = new ymaps.Circle([[pack.lat, pack.lng], pack.radius], {}, {
        fillColor: "#DB709377",
        strokeColor: "#990066",
        strokeOpacity: 0.8,
        strokeWidth: 1
    });
    map.geoObjects.add(last);
    var bounds = last.geometry._bounds;
    map.setBounds(bounds, { checkZoomRange: true });
    if(debug) {
        console.log("load mark from model: ", pack.lat, pack.lng, "r", pack.radius);
    }
    map.events.add('click', function (e) {
        var coords = e.get('coords');
        var lat = coords[0].toPrecision(precision);
        var lng = coords[1].toPrecision(precision);
        pack.lat = lat;
        pack.lng = lng;
        var myGeocoder = ymaps.geocode([lat, lng]);
        myGeocoder.then(
            function (res) {
                var nearest = res.geoObjects.get(0);
                var address = nearest.properties.get('text');
                pack.address = address;
                $('#address-field').val(pack.address);
                if(debug) {
                    console.log("address picked", address);
                }
                updateData(pack);
            },
            function (err) {
                alert('Ошибка');
            }
        );
        var mark = new ymaps.Circle([[pack.lat, pack.lng], pack.radius], {}, {
            fillColor: "#DB709377",
            strokeColor: "#990066",
            strokeOpacity: 0.8,
            strokeWidth: 1
        });
        if(last) {
            map.geoObjects.remove(last);
            if(debug) {
                console.log("remove last mark");
            }
        }
        last = mark;
        map.geoObjects.add(mark);
        if(debug) {
            console.log("map click coord: ", lat, lng, "r", pack.radius);
        }
        updateData(pack);
    });

    $('#create-button').click(function(event) {
        event.preventDefault();
        if(debug) {
            console.log("click update btn");
        }
        if(old_pack.lat != pack.lat || old_pack.lng != pack.lng) {
            if(debug) {
                console.log("delete old point and add new");
            }
            delOldPoint(pack.point_id, createPoint);
        } else {
            if(debug) {
                console.log("old point save meta data");
            }
            $('#location-form').submit();
        }
    });

    function delOldPoint(point_id, callBack) {
        $.post(point_delete_url, {point_id:point_id}, function(answer) {
            if(answer.success) {
                if(debug) {
                    console.log("old point deleted succeful", answer);
                }
                callBack();
            } else {
                if(debug) {
                    console.log("cant delete old point", answer);
                }
            }
        });
    }

    function createPoint() {
        $.post(point_create_url, {user_id:locations_owner_id, lat: pack.lat, lng: pack.lng}, function(answer) {
            if(answer.success) {
                if(debug) {
                    console.log("point added", answer);
                }
                $('#point-field').val(answer.id);
                $('#location-form').submit();
            } else {
                if(debug) {
                    console.log("cant add point", answer);
                }
            }
        });
    }

    function updateData(obj) {
        $('#name').text(obj.name);
        $('#address').text(obj.address);
        $('#radius').text(obj.radius);
        $('#point').text("lat: " + obj.lat + " lng: " + obj.lng);
        $('#is_banned').text(obj.is_banned ? "yes" : "no");
    }

    $('#name-field').on('keyup', function(event){
        pack.name = $(this).val();
        if(debug) {
            console.log("name changed", pack.name);
        }
        updateData(pack);
    });

    $('#radius-field').on('keyup', function(event){
        pack.radius = $(this).val();
        if(debug) {
            console.log("radius changed", pack.radius);
        }
        updateData(pack);
        last.geometry.setRadius(pack.radius);
    });

    $('#address-field').on('keyup', function(event){
        pack.address = $(this).val();
        if(debug) {
            console.log("address changed", pack.address);
        }
        updateData(pack);
    });

    $('#ban-field').on('change', function(event){
        pack.is_banned = $(this).val();
        if(debug) {
            console.log("ban status changed", pack.is_banned);
        }
        updateData(pack);
    });
    updateData(pack);
});
