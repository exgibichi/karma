$(document).ready(function(){
    $('#setting-name').on('keyup', function() {
        var alias = $(this).val().toLocaleLowerCase().split(' ').join('_').split('  ').join('_');
        $('#setting-alias').val(alias);
    });
});
