$(document).ready(function() {

    $('#first-point-btn').click(function(event) {
        if(first_point.coords != undefined) {
            points = [];
            updatePoints();
            addMark(first_point.coords, first_point.is_like);
            map.setCenter(first_point.coords);
        }
    });

    $('#last-point-btn').click(function(event) {
        if(last_point.coords != undefined) {
            points = [];
            updatePoints();
            addMark(last_point.coords, last_point.is_like);
            map.setCenter(last_point.coords);
        }
    });

});
