function updateData(obj) {
    $('#name').text(obj.name);
    $('#address').text(obj.address);
    $('#radius').text(obj.radius);
    $('#base_points_cnt').text(obj.base_points_cnt);
    $('#point').text("lat: " + obj.lat + " lng: " + obj.lng);
    $('#is_banned').text(obj.is_banned ? "yes" : "no");
}
updateData(pack);
