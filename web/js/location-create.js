ymaps.ready(function () {
    var map = new ymaps.Map("map", {
        center: [55.76, 37.64],
        zoom: 10,
        controls: ["zoomControl", "fullscreenControl", "geolocationControl", "searchControl", "typeSelector"]
    });
    var last;
    var mark;
    map.events.add('click', function (e) {
        var coords = e.get('coords');
        var lat = coords[0].toPrecision(precision);
        var lng = coords[1].toPrecision(precision);
        pack.lat = lat;
        pack.lng = lng;
        var myGeocoder = ymaps.geocode([lat,lng]);
        myGeocoder.then(
            function (res) {
                var nearest = res.geoObjects.get(0);
                var address = nearest.properties.get('text');
                pack.address = address;
                $('#address-field').val(pack.address);
                if(debug) {
                    console.log("address picked", address);
                }
                updateData(pack);
            },
            function (err) {
                alert('Ошибка');
            }
        );
        mark = new ymaps.Circle([[lat, lng], pack.radius], {}, {
            fillColor: "#DB709377",
            strokeColor: "#990066",
            strokeOpacity: 0.8,
            strokeWidth: 1
        });
        if(last) {
            map.geoObjects.remove(last);
            if(debug) {
                console.log("remove last mark");
            }
        }
        last = mark;
        map.geoObjects.add(mark);
        if(debug) {
            console.log("map click coord: ", lat, lng, "r", pack.radius);
        }
        updateData(pack);
    });
    setDefault();

    $('#create-button').click(function(event){
        event.preventDefault();
        $.post(point_create_url, {user_id:locations_owner_id, lat: pack.lat, lng: pack.lng}, function(answer){
            if(answer.success) {
                if(debug) {
                    console.log("point added", answer);
                }
                $('#point-field').val(answer.id);
                $('#location-form').submit();
            } else {
                if(debug) {
                    console.log("cant add point", answer);
                }
            }
        });
    });

    function setDefault() {
        if(pack.radius === undefined) {
            $('#radius-field').val(default_radius);
            pack.radius = default_radius;
        }
        if(pack.lat === undefined) {
            pack.lat = 0;
        }
        if(pack.lng === undefined) {
            pack.lng = 0;
        }
        if(pack.is_banned === undefined) {
            pack.is_banned = 0;
        }
        updateData(pack);
    }

    function updateData(obj) {
        $('#name').text(obj.name);
        $('#address').text(obj.address);
        $('#radius').text(obj.radius);
        $('#point').text("lat: " + obj.lat + " lng: " + obj.lng);
        $('#is_banned').text(obj.is_banned ? "yes" : "no");
    }

    $('#name-field').on('keyup', function(event){
        pack.name = $(this).val();
        if(debug) {
            console.log("name changed", pack.name);
        }
        updateData(pack);
    });
    $('#radius-field').on('keyup', function(event){
        pack.radius = $(this).val();
        if(debug) {
            console.log("radius changed", pack.radius);
        }
        updateData(pack);
        mark.geometry.setRadius(pack.radius);
    });
    $('#address-field').on('keyup', function(event){
        pack.address = $(this).val();
        if(debug) {
            console.log("address changed", pack.address);
        }
        updateData(pack);
    });
    $('#ban-field').on('change', function(event){
        pack.is_banned = $(this).val();
        if(debug) {
            console.log("ban status changed", pack.is_banned);
        }
        updateData(pack);
    });
});
