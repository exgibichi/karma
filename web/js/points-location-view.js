ymaps.ready(function () {
    loadCircle();
});

function loadCircle() {
    circle = new ymaps.Circle([location_pack.center, location_pack.radius], {}, {
        fillColor: "#DB709377",
        strokeColor: "#990066",
        interactivityModel: "default#silent",
        strokeOpacity: 0.8,
        strokeWidth: 1
    });
    map.geoObjects.add(circle);
    var bounds = circle.geometry._bounds;
    map.setBounds(bounds, { checkZoomRange: true });
}
