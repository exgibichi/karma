function updateData(obj) {
    $('#name').text(obj.name);
    $('#address').text(obj.address);
    $('#radius').text(obj.radius);
    $('#point').text("lat: " + obj.lat + " lng: " + obj.lng);
    $('#is_banned').text(obj.is_banned ? "yes" : "no");
}
updateData(pack)
ymaps.ready(function () {
    var map = new ymaps.Map("map", {
        center: [pack.lat, pack.lng],
        zoom: 15,
        controls: ["zoomControl", "fullscreenControl", "typeSelector"]
    });
    var mark = new ymaps.Circle([[pack.lat, pack.lng], pack.radius], {}, {
        fillColor: "#DB709377",
        strokeColor: "#990066",
        strokeOpacity: 0.8,
        strokeWidth: 1
    });
    map.geoObjects.add(mark);
});
