function updatePoints() {
    points.sort(function(a,b){
        return a.id - b.id;
    });
    if(debug) {
        console.log('points', points);
    }
    var html_tpl = `<table class="table table-bordered">
    <thead>
    <tr>
    <th>#</th>
    <th>Point</th>
    <th>
    Like
    <a class="like_all_points" style="font-size:15pt;margin:3pt;">
    <span class="fa fa-thumbs-o-up text-success"></span>
    </a>
    <a class="dislike_all_points" style="font-size:15pt;margin:3pt;">
    <span class="fa fa-thumbs-o-down text-danger"></span>
    </a>
    </th>
    <th>
    Actions
    <a class="delete_all_points" style="font-size:15pt;margin:3pt;">
    <span class="fa fa-trash text-danger"></span>
    </a>
    </th>
    </tr>
    </thead>
    <tbody>
    {{points}}
    </tbody>
    </table>`;
    var points_tpl = `<tr>
    <td>{{id}}</td>
    <td>lat: {{lat}} lng: {{lng}}</td>
    <td>
    {{is_like}}
    </td>
    <td>
    <a id="point_{{id}}" class="like_point" style="font-size:15pt;margin:3pt;">
    <span class="fa fa-thumbs-o-up text-success"></span>
    </a>
    <a id="point_{{id}}" class="dislike_point" style="font-size:15pt;margin:3pt;">
    <span class="fa fa-thumbs-o-down text-danger"></span>
    </a>
    <a id="point_{{id}}" class="delete_point" style="font-size:15pt;margin:3pt;">
    <span class="fa fa-trash text-danger"></span>
    </a>
    </td>
    </tr>`;
    var points_html = '';
    $.each(points, function(index, point) {
        if(point.is_deleted == 1) {
            return;
        }
        points_html += points_tpl;
        $.each(point, function(key, value) {
            if(key == "is_like") {
                points_html = points_html.replace(
                    '{{is_like}}',
                    value ?
                    '<span class="fa fa-thumbs-o-up text-success"></span>' :
                    '<span class="fa fa-thumbs-o-down text-danger"></span>'
                );
            } else if(key == "id") {
                points_html = points_html.split('{{id}}').join(value);
            } else {
                points_html = points_html.replace('{{' + key + '}}', value);
            }
        });
    });
    $('#points').html(html_tpl.replace('{{points}}', points_html));
}

$('#points').on('click', '.like_point', function() {
    var id = $(this).prop('id').replace('point_', '');
    likePointById(id);
    updatePoints();
});

$('#points').on('click', '.dislike_point', function() {
    var id = $(this).prop('id').replace('point_', '');
    dislikePointById(id);
    updatePoints();
});

$('#points').on('click', '.like_all_points', function() {
    likeAllPoints();
    updatePoints();
});

$('#points').on('click', '.dislike_all_points', function() {
    dislikeAllPoints();
    updatePoints();
});

$('#points').on('click', '.delete_all_points', function() {
    deleteAllPoints();
    updatePoints();
});

$('#points').on('click', '.delete_point', function() {
    var id = $(this).prop('id').replace('point_', '');
    deletePointById(id);
    updatePoints();
});

$('#points-tbl').on('click', '#save-points-button', function() {
    if(!points.length) { return; }
    var raw_points = getRawPoints();
    if(debug) {
        console.log('rp', raw_points);
    }
    $.post(points_bulk_add_url, {points: raw_points}, function(answer) {
        if(answer.success) {
            alert('points saved');
            $.each(answer.saved_points, function(i, v) {
                var point = getPointById(v.id);
                point.is_like = parseInt(v.is_like);
                point.base_id = v.base_id;
                updatePointById(v.id, point);
            });
            updatePoints();
        }
    });
});

function _objectWithoutProperties(obj, keys) {
    var target = {};
    for (var i in obj) {
        if (keys.indexOf(i) >= 0) continue;
        if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
        target[i] = obj[i];
    }
    return target;
}

function getRawPoints() {
    var raw_points = [];
    $.each(points, function(i,v) {
        var point = _objectWithoutProperties(v, ['point_obj']);
        raw_points.push(point);
    });
    return raw_points;
}

function getPointById(id, invert = 0) {
    if(!invert) {
        return $.grep(points, function(p){
            return p.id == id;
        })[0];
    } else {
        return $.grep(points, function(p){
            return p.id != id;
        });
    }
}

function deletePointById(id) {
    var point = getPointById(id);
    map.geoObjects.remove(point.point_obj);
    point.is_deleted = 1;
    updatePointById(id, point);
}

function likePointById(id) {
    var point = getPointById(id);
    point.is_like = 1;
    point.point_obj.options.set({preset: 'islands#greenCircleDotIcon'});
    updatePointById(id, point);
}

function dislikePointById(id) {
    var point = getPointById(id);
    point.is_like = 0;
    point.point_obj.options.set({preset: 'islands#redCircleDotIcon'});
    updatePointById(id, point);
}

function updatePointById(id, point) {
    points = getPointById(id, 1);
    points.push(point);
}

function likeAllPoints() {
    $.each(points, function(i, v) {
        points[i].is_like = 1;
        points[i].point_obj.options.set({preset: 'islands#greenCircleDotIcon'});
    });
}

function dislikeAllPoints() {
    $.each(points, function(i, v) {
        points[i].is_like = 0;
        points[i].point_obj.options.set({preset: 'islands#redCircleDotIcon'});
    });
}

function deleteAllPoints() {
    $.each(points, function(i, v) {
        deletePointById(v.id);
    });
}
