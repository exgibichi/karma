ymaps.ready(function () {
    loadMap();
    loadPoints();
});

$(document).ready(function() {
    $('#filter-btn').click(function(event) {
        event.preventDefault();
        getCurrentMapData();
        var form = $("#filter-form");
        var action = form.prop('action');
        var data = form.serialize();
        $.post(action, data, function(response) {
            if(response.success) {
                points = response.points;
                count = response.count;
                center = center;
                updatePoints();
            }
        });
    });
    if($('#time-filter-field').val() == custom_id) {
        $("#time-range").show();
    } else {
        $("#time-range").hide();
    }
    $('#time-filter-field').change(function(e) {
        if($(this).val() == custom_id) {
            $("#time-range").show();
        } else {
            $("#time-range").hide();
        }
    });
});

function loadMap() {
    if(debug) {
        console.log("load map");
        console.log("map center: ", center.length > 0 ? 'points center' : 'default center');
    }
    map = new ymaps.Map("map", {
        center: center.length > 0 ? center : [default_lat, default_lng],
        zoom: 10,
        controls: ["zoomControl", "fullscreenControl", "typeSelector"]
    });
}

function addMark(coord, is_like) {
    is_like = parseInt(is_like);
    lat = parseFloat(coord[0]);
    lng = parseFloat(coord[1]);
    var point = new ymaps.Placemark([lat, lng], {}, {
        preset: is_like ? 'islands#greenCircleDotIcon' : 'islands#redCircleDotIcon'
    });
    map.geoObjects.add(point);
    current_points.push({
        point_obj: point
    });
    if(debug) {
        console.log("map add mark: ", lat, lng);
    }
}

function updatePoints() {
    if(current_points.length > 0) {
        $.each(current_points, function(i, v) {
            map.geoObjects.remove(v.point_obj);
        });
    }
    loadPoints();
}

function loadPoints() {
    if(debug) {
        console.log("load points");
        console.log("points count: ", points.length);
    }
    $('#filtered').text(count);
    $('#showed').text(points.length);
    if(points.length > 0) {
        $.each(points, function(i, v) {
            addMark([v.lat, v.lng], v.is_like);
        });
    }
}

function getCurrentMapData() {
    center = map.getCenter();
    var lat_min = map._bounds[1][0];
    var lat_mid = ((lat_min + lat_min)/2);
    var radius = (lat_mid-center[0])*one_degree_in_meters;
    $('#radius-field').val(radius);
    $('#cc-field').val(center[0].toPrecision(precision)+','+center[1].toPrecision(precision));
}
