$('#gen-options').on('click', '#gen-points-button', function() {
    getOptions();
    generatePoints();
});

function getOptions() {
    gen_options.cnt = parseInt($("#cnt").val());
    gen_options.likes_percent = parseFloat($("#likes_percent").val());
    gen_options.dislikes_percent = parseFloat($("#dislikes_percent").val());
    gen_options.lat = parseFloat(pack.lat);
    gen_options.lng = parseFloat(pack.lng);
    gen_options.radius = (pack.radius/one_degree_in_meters).toPrecision(precision);
    gen_options.poi = points_owner_id;
}

function generatePoints() {
    if(debug) {
        console.log("gen-options", gen_options);
    }
    if(gen_options.cnt > 100) {
        generateOnServer();
    } else {
        for (var i = 0; i < gen_options.cnt; i++) {
            addMark(generatePoint(), Math.random()<(gen_options.likes_percent/100) ? 1 : 0, null, points_owner_id);
        }
    }
}

function generateOnServer() {
    $.post(gen_points_url, gen_options, function(data) {
        if(debug) {
            console.log(data);
        }
        data = JSON.parse(data);
        if(data.cnt != undefined) {
            alert('generated points: '+data.cnt);
            location.reload();
        }
    });
}

function generatePoint() {
    var theta = Math.random() * 2 * Math.PI;
    var distance = Math.sqrt(Math.random()) * gen_options.radius;
    var x = distance * Math.cos( theta ) + gen_options.lat;
    var y = distance * Math.sin( theta ) + gen_options.lng;
    if(debug) {
        console.log("generate point x:", x, "y:", y);
    }
    return [x.toPrecision(precision), y.toPrecision(precision)];
}
